<?php
session_start();
include("functions.inc.php");

$action = $_GET['action'];
$id = $_GET['id'];
$name = $_GET['name'];



$gefunden = 0;

if (isset($_POST['fsenden']) && logged_in() && $_SESSION['kontrolle']==$_POST['kontrolle']){
    $_SESSION['kontrolle'] = genRandomString();
    $bid = $_POST['id'];
    $bname = $_POST['name'];
    $binhalt = mysql_real_escape_string($_POST['inhalt']);
    if ($bid==0) {
        $idres = mysql_query("SELECT MAX(id) FROM `seiten`") or die(mysql_error());
        if ($idarr = mysql_fetch_array($idres)) {
            $neueid = $idarr[0]+1;
        } else {
            $neueid = 1;
        }        
        mysql_query("INSERT INTO seiten (`id`, `name`, `inhalt`, `autor`, `version`) VALUES ($neueid, '$bname', '$binhalt', '".$_SESSION['benutzer']."', 0)") or die(mysql_error());
        $id = $neueid;
        $action = 1;
    } else {
        $maxres = mysql_query("SELECT MAX(version) FROM `seiten` WHERE `id`=$bid") or die(mysql_error());
        if ($neuenr = mysql_fetch_array($maxres)) {
            $neueversion = $neuenr[0]+1;
        } else {
            $neueversion = 0;
        }
        mysql_query("INSERT INTO seiten (`id`, `name`, `inhalt`, `autor`, `version`) VALUES ('$bid', '$bname', '$binhalt', '".$_SESSION['benutzer']."', $neueversion)") or die(mysql_error());
        $id=$bid;
        $action=1;
    }
}

if (!isset($id) && !isset($name)) { $id=1; }


if (!isset($id)) {
    $result = mysql_query("SELECT * FROM seiten WHERE `name`='$name' ORDER BY `version` DESC") or die("Seite mit diesem Namen nicht gefunden.");
} else {
    if (isset($_GET['version'])) {
        $version=$_GET['version'];
        $result = mysql_query("SELECT * FROM seiten WHERE id=$id AND version=$version ORDER BY `version` DESC") or die(mysql_error());
        if (mysql_numrows($result)<1){
            $gefunden = 99;
        }
    } else {
        $result = mysql_query("SELECT * FROM seiten WHERE id=$id ORDER BY `version` DESC") or die(mysql_error());        
    }
}

if (mysql_numrows($result)>0 && $gefunden!=99){
    $gefunden = 1;
    $seite = mysql_fetch_array($result);
    $name = $seite['name'];
    $id = $seite['id'];
}

$_SESSION['seitenid'] = $id;

if ($action != 2) {  //anzeigen
    if ($gefunden == 1){
        if (logged_in()){
            $status = '<a style="color:black" href="login.php">'.$_SESSION['benutzer'].'</a>('.$_SESSION['stufe'].')';
            
            include('menu.php');
            
            if (stripos($seite['inhalt'],"</body>")!=null) {
                $mitnavi = str_ireplace("</body>", $htmlwikimenu."</body>", stripslashes($seite['inhalt']));
                $mitnavi = str_ireplace('</head>', '<link rel="stylesheet" type="text/css" href="menu.css" /> <!-- HTMLWiki-Navigationsleiste -->'."\n".'</head>', $mitnavi);
                echo $mitnavi;
            } else {
                echo stripslashes($seite['inhalt']);
                echo $meinmenu;
            }
        } else {
            echo stripslashes(str_replace("%navi%","",$seite['inhalt']));
        }
    } else {
        if (logged_in() && $gefunden!=99){
            $action = 2;
        } else {
            if ($gefunden==99){
                echo '<p>Seitenversion nicht gefunden!</p>';
            } else {
                echo '<p>Seite nicht gefunden!</p>';                
            }
        }
    }
}

if (($action == 2) && logged_in()) {  //bearbeiten
    echo '
    <html>
    <head>
        <script src="lib/codemirror/js/codemirror.js" type="text/javascript"></script>
        <script src="lib/codemirror/js/mirrorframe.js" type="text/javascript"></script>
        <title>Bearbeite Seite "'.$name.'" (ID '.$id.')</title>
        <style type="text/css">
            .CodeMirror-line-numbers {
                width: 2.2em;
                color: #aaa;
                background-color: #eee;
                text-align: right;
                padding-right: .3em;
                font-size: 10pt;
                font-family: monospace;
                padding-top: .4em;
            }
        </style>
    </head>
    <body>
    <h1>Bearbeite Seite "'.$name.'" (ID '.$id.')</h1>
    ';
    if ($gefunden == 0) {
        echo "<p>Seite existiert noch nicht!</p>";
        $id = 0;
    }
    $kontrollstring = genRandomString();
    $_SESSION['kontrolle'] = $kontrollstring;
    echo '
    <form action="index.php?name='.$name.'&amp;action=1" method="post">
        <input type="hidden" name="kontrolle" value="'.$kontrollstring.'">
        <input type="hidden" name="id" value="'.$id.'">
        Name: '.$name.'<input type="hidden" name="name" value="'.$name.'">.htm<br />
        <textarea id="finhalt" name="inhalt" cols="50" rows="10">'.stripslashes($seite['inhalt']).'</textarea><br />
        <input type="submit" name="fsenden" value="speichern" />
        <input type="reset" value="zur&uuml;cksetzen" />
    </form>
    <script type="text/javascript">
        var textarea = document.getElementById("finhalt");
        var editor = new MirrorFrame(CodeMirror.fromTextArea(textarea, {
            content: textarea.value,
            parserfile: ["parsexml.js", "parsecss.js", "tokenizejavascript.js", "parsejavascript.js", "parsehtmlmixed.js"],
            stylesheet: ["lib/codemirror/css/xmlcolors.css", "lib/codemirror/css/jscolors.css", "lib/codemirror/css/csscolors.css"],
            path: "lib/codemirror/js/",
            autoMatchParens: true,
            lineNumbers: true
        }));
    </script>
    </body>
    </html>
    ';    
    
}

?>