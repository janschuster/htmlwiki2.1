<?php
session_start();
echo '<?xml version="1.0" ?>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>HTMLWiki - Login</title>
    <link rel="stylesheet" type="text/css" href="htmlwiki.css" />
</head>
<body>
<h1>Login zum HTMLWiki</h1>

<?php
include("functions.inc.php");

if (isset($_POST['login']) && $_SESSION['kontrolle']==$_POST['kontrolle']){
    if (login($_POST['benutzer'],$_POST['benutzerpw'])==true){
        echo '<p>Erfolgreich angemeldet!</p>';
    } else {
        echo '<p>Anmeldedaten fehlerhaft!</p>';
    }
}

if (isset($_POST['logout']) && $_SESSION['kontrolle']==$_POST['kontrolle']){
    logout();
    echo '<p>Erfolgreich abgemeldet!<br />
          <a href="index.php">Zur Startseite</a></p>';
}

if (logged_in()){
    $kontrollstring = genRandomString();
    $_SESSION['kontrolle'] = $kontrollstring;
    echo '
    <p>Sie sind eingelogged als '.$_SESSION['vorname'].' '.$_SESSION['nachname'].' ('.$_SESSION['benutzer'].'), Stufe '.$_SESSION['stufe'].'. Klicken Sie auf den folgenden Button um sich auszuloggen.</p>
    <a href="index.php">Zur Startseite</a>
    <form action="" method="post">
        <p><input type="hidden" name="kontrolle" value="'.$kontrollstring.'" />
        <input type="submit" name="logout" value="abmelden" /></p>
    </form>
    ';
} else {
    $kontrollstring = genRandomString();
    $_SESSION['kontrolle'] = $kontrollstring;
    echo '
    <p>Bitte geben Sie Ihre Zugangsdaten ein um sich anzumelden:</p>
    <form action="" method="post">
        <table>
            <tr>
                <td>
                    <input type="hidden" name="kontrolle" value="'.$kontrollstring.'" />
                    <label for="iben">Benutzername:</label>
                </td>
                <td>
                    <input type="text" name="benutzer" id="iben" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="ipwd">Passwort:</label>
                </td>
                <td>
                    <input type="password" name="benutzerpw" id="ipwd" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" name="login" value="anmelden" /> <input type="reset" />
                </td>
            </tr>
        </table>
    </form>
    ';
}
if (logged_in()){
    $status = '<a href="login.php">'.$_SESSION['benutzer'].'</a>('.$_SESSION['stufe'].')';
    $nav = '
<!-- Navigationszeile Anfang -->
    <p>
        '.$status.' |
        <a href="index.php">Zur Startseite</a> |
        <a href="inhalt.php">Inhaltsverzeichnis</a> |
        <a href="upload.php">Bilder hochladen</a> |
    </p>
<!-- Navigationszeile Ende -->
';
}

?>

</body>
</html>
