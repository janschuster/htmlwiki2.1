<?php session_start();
echo '<?xml version="1.0" ?>
';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Datei-Upload</title>
        <link rel="stylesheet" type="text/css" href="htmlwiki.css" />
    </head>
    <body>
        

<?php
include("functions.inc.php");

echo '<h1>Datei-Upload</h1>';

if (!logged_in()){
    echo '<p>Diese Funktion ist nur f�r angemeldeten Benutzern verf�gbar.</p>';
} else {
if (!isset($_POST['fhoch'])) {
    echo '
    <p>Es sind nur Bilddateien bis zu einer Gr��e von 1MB erlaubt.</p>
    <form action="upload.php" method="post" enctype="multipart/form-data">
    <p>
        <input type="file" name="datei" /><br />
        <input type="submit" value="Hochladen" name="fhoch" />
    </p>
    </form>';
    echo '
    <h2>Bereits vorhandene Dateien:</h2>
    <p>Einbinden mit "files/"+Name</p>';
    $ordner = "files/";
    $dir_handle = opendir($ordner);
    echo '
    <table border="1">
        <tr>
            <th>Dateiname</th>
            <th>verwendet in</th>
        </tr>
    ';
    while ($filename = readdir($dir_handle)) {
        if ($filename != '..' && $filename != '.') {
            echo '<tr>';
            $fresult = mysql_query("SELECT DISTINCT `name` FROM `seiten` WHERE `inhalt` LIKE '%files/$filename%'") or die(mysql_error());
            echo '<td><a href="files/'.$filename.'">'.$filename.'</a></td>
                  <td>';
            while ($ref = mysql_fetch_array($fresult)){
                echo '
                    <a href="'.$ref['name'].'.htm">'.$ref['name'].'</a> ';
            }
            echo '
                  </td>
                  </tr>';
        }
    }
    echo '</table>';
    closedir($dir_handle);

} else {

$dateityp = GetImageSize($_FILES['datei']['tmp_name']);
if($dateityp[2] != 0)
   {

   if($_FILES['datei']['size'] <  1024000)
      {
      move_uploaded_file($_FILES['datei']['tmp_name'], "files/".$_FILES['datei']['name']);
      echo '<p>Das Bild wurde erfolgreich nach <a href="files/'.$_FILES['datei']['name'].'">files/'.$_FILES['datei']['name'].'</a> hochgeladen.</p>';
      echo '<p><img src="files/'.$_FILES['datei']['name'].'" alt="hochgeladenes Bild" /></p>';
      }

   else
      {
         echo "Das Bild darf nicht gr��er als 1000 kb sein ";
      }

    }

else
    {
    echo "Bitte nur Bilder im gif, png oder jpg Format hochladen";
    }
}

} //Ende if logged_in

if (logged_in()){
    $status = '<a href="login.php">'.$_SESSION['benutzer'].'</a>('.$_SESSION['stufe'].')';
    $nav = '
<!-- Navigationszeile Anfang -->
    <hr />
    <p>
        '.$status.' |
        <a href="index.php">Zur Startseite</a> |
        <a href="inhalt.php">Inhaltsverzeichnis</a>
    </p>
<!-- Navigationszeile Ende -->
';

    echo $nav;
} else {
    echo '<p><a href="login.php">Anmelden</a></p>';
}

?>

    </body>
    
</html>
